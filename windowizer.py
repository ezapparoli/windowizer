#!/usr/bin/python

from __future__ import print_function
import argparse
import errno
import logging
import requests
import sys
from string import Template

def chr_split (**kwargs):

	out_file = kwargs.get('outfile', '')
	intervallo = kwargs.get('interval', '')
	chr_num = kwargs.get('chr', '')
	lunghezza_chr = kwargs.get('length', '')
	out_file = kwargs.get('outfile', '')

	if args.outfile:
	   handler = open(out_file,"w")

	finestretot = lunghezza_chr // intervallo
	tronca = lunghezza_chr % intervallo
	if tronca != 0:
	   finestretot = finestretot + 1

	limit = range(1,finestretot)
	for count in limit:
	   
	   destra = count * intervallo
	   sinistra = destra - (intervallo - 1)

	   out = "chr%02g:%s..%s\n" % (chr_num,sinistra,destra)
	   if not args.outfile or args.silent is False:
		  print(out,end = '')
	   if args.outfile:
		  handler.write(out)

	if destra != lunghezza_chr:
	   count = count + 1
	   
	   sinistra = destra + 1
	   destra = count * intervallo
	   
	   out = "chr%02g:%s..%s\n" % (chr_num,sinistra,lunghezza_chr)
	   if not args.outfile or args.silent is False:
		  print(out,end = '')
	   if args.outfile:
		  handler.write(out)

	if args.outfile:
	   handler.close()

if __name__ == "__main__":
   parser = argparse.ArgumentParser(description='Custom splitting of single chromosome coordinates.')
   parser.add_argument("-o", "--outfile", dest="outfile", help="output file name", action="store", type=str)
   parser.add_argument("-s", "--silent", dest="silent", help="doesn't print output while an outfile is given", action="store_true")
   parser.add_argument("-i", "--interval", dest="interval", help="interval size", action="store", type=int, required=True)
   parser.add_argument("-c", "--chr", dest="chr", help="chromosome number", action="store", type=int, required=True)
   parser.add_argument("-l", "--length", dest="length", help="chromosome length", action="store", type=int, required=True)

   args = parser.parse_args()

   chr_split(interval=args.interval,
					chr=args.chr,
					length=args.length,
					outfile=args.outfile)
